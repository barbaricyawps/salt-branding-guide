# Salt Branding Guide

Salt Project (saltproject.io) logos and branding-usage guidance.

**Salt Project logos can be used freely when referencing the Salt Project. Logos may be resized within scale only. When placing the logo on any background other than white, please use the vertical logo options when possible for best results. Please contact the Salt Community Manager at janaea@vmware.com with any specific questions about logo use.**

[[_TOC_]]

## Typography / Fonts

> `saltproject.io` uses the _Work Sans_ font family: [Download the _Work Sans_ font family from Google](https://fonts.google.com/specimen/Work+Sans)

**Headers / subheaders**
- Work Sans, Bold

Body text
- Work Sans, Light

## Color Palette

![Color Palette](./misc/color-palette.png "Color Palette Examples")

**Black**

- #151616
- RGB 21/22/22
- CMYK 73/67/65/80

**Blue**

- #0091da
- RGB 0/145/218
- CMYK 77/32/0/0

**Cream**

- #ede3de
- RGB 237/227/222
- CMYK 6/9/10/0

**Teal**

- #57bcad
- RGB 87/188/173
- CMYK 63/3/38/0

## Logo Types

Here is a quick reference to all the available logos/branding images in this repository.

> _Note: The **black** and **cream** might will be difficult to see on certain website backdrops, depending on whether the standard or dark themes are being used. The Salt Project most often defaults to **Teal** logos, but a variety of colors are presented for flexibility._

### Full Lockup

![Salt Project Logo - Full Lockup - Black](./logos/SaltProject_lockup_black.png "Salt Project Logo - Full Lockup - Black")

![Salt Project Logo - Full Lockup - Blue](./logos/SaltProject_lockup_blue.png "Salt Project Logo - Full Lockup - Blue")

![Salt Project Logo - Full Lockup - Cream](./logos/SaltProject_lockup_cream.png "Salt Project Logo - Full Lockup - Cream")

![Salt Project Logo - Full Lockup - Teal](./logos/SaltProject_lockup_teal.png "Salt Project Logo - Full Lockup - Teal")

### Horizontal Lockup

![Salt Project Logo - Horizontal Lockup - Black](./logos/SaltProject_altlogo_black.png "Salt Project Logo - Horizontal Lockup - Black")

![Salt Project Logo - Horizontal Lockup - Blue](./logos/SaltProject_altlogo_blue.png "Salt Project Logo - Horizontal Lockup - Blue")

![Salt Project Logo - Horizontal Lockup - Cream](./logos/SaltProject_altlogo_cream.png "Salt Project Logo - Horizontal Lockup - Cream")

![Salt Project Logo - Horizontal Lockup - Teal](./logos/SaltProject_altlogo_teal.png "Salt Project Logo - Horizontal Lockup - Teal")

### Vertical Lockup

![Salt Project Logo - Vertical Lockup - Black](./logos/SaltProject_verticallogo_black.png "Salt Project Logo - Vertical Lockup - Black")

![Salt Project Logo - Vertical Lockup - Blue](./logos/SaltProject_verticallogo_blue.png "Salt Project Logo - Vertical Lockup - Blue")

![Salt Project Logo - Vertical Lockup - Cream](./logos/SaltProject_verticallogo_cream.png "Salt Project Logo - Vertical Lockup - Cream")

![Salt Project Logo - Vertical Lockup - Teal](./logos/SaltProject_verticallogo_teal.png "Salt Project Logo - Vertical Lockup - Teal")

### Logomark

![Salt Project Logo - Logomark - Black](./logos/SaltProject_Logomark_black.png "Salt Project Logo - Logomark - Black")

![Salt Project Logo - Logomark - Blue](./logos/SaltProject_Logomark_blue.png "Salt Project Logo - Logomark - Blue")

![Salt Project Logo - Logomark - Cream](./logos/SaltProject_Logomark_cream.png "Salt Project Logo - Logomark - Cream")

![Salt Project Logo - Logomark - Teal](./logos/SaltProject_Logomark_teal.png "Salt Project Logo - Logomark - Teal")
